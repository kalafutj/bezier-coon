#include "bod.h"



bod::bod()
{
}

bod::bod(double X, double Y) {
	x = X;
	y = Y;
}

bod::bod(QPoint A) {
	x = A.x();
	y = A.y();
}

bod bod::operator*(double k) {
	return(bod(k*x, k*y));
}

bod bod::operator+(bod A) {
	return(bod(x + A.x, y + A.y));
}

bod::~bod()
{
}
