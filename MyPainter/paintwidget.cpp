#include "paintwidget.h"
#include<iostream>


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

//nacita body(lave),vykresli(prave)
void PaintWidget::mousePressEvent(QMouseEvent *event) {
	if (event->buttons() == Qt::LeftButton)
		V.push_back(bod(event->pos()));
	if (event->buttons() == Qt::RightButton) {
		if (BezierCoons)coons();
		else bezier();
		V.clear();
	}
	update();
}

bool PaintWidget::mimo(int x, int y) {
	if (x < 0) return true;
	if (x >= image.width())return true;
	if (y < 0)return true;
	if (y >= image.height())return true;
	return false;
}

void PaintWidget::dda(int zx, int zy, int kx, int ky) {

	//std::cout << zx << std::endl;
	if (abs(ky - zy) > abs(kx - zx)) {
		double k = (kx - zx) / (double)(ky - zy);
		double q = kx - k*ky;
		if (zy > ky) for (int y = zy; y > ky; y--) if (!mimo(k*y + q, y)) image.setPixelColor(k*y + q, y, QColor(0, 0, 0));
		else for (int y = zy; y < ky; y++) if (!mimo(k*y + q, y)) image.setPixelColor(k*y + q, y, QColor(0, 0, 0));
	}
	else {
		double k = (ky - zy) / (double)(kx - zx);
		double q = ky - k*kx;
		if (zx > kx)for (int x = zx; x > kx; x--) if (!mimo(x, k*x + q)) image.setPixelColor(x, k*x + q, QColor(0, 0, 0));
		else for (int x = zx; x < kx; x++) if (!mimo(x, k*x + q)) image.setPixelColor(x, k*x + q, QColor(0, 0, 0));
	}
	if (zx == kx&&zy == ky&&!mimo(kx,zy))image.setPixelColor(zx, ky, QColor(0, 0, 0));

}

void PaintWidget::bezier() {
	double dt = 0.00001;
	int n = V.size();
	bod *P = new bod[n*n];
	bod B = V[0];

	for (int i = 0; i < n; i++) P[i*n] = V[i];


	for (double t = 0; t < 1+dt; t += dt) {

		for (int j = 1; j < n; j++)
			for (int i = 0; i < n - j; i++)
				P[i*n + j] = P[i*n + j - 1] * (1 - t) + P[(i + 1)*n + j - 1] * t;

		dda(round(B.x), round(B.y), round(P[n-1].x), round(P[n-1].y));
		B = P[n-1];

	}
	std::cout << std::endl;
}

void PaintWidget::coons() {
	bod X,Y;
	//prchadzam vektor bodov
	for (int i = 0; i < V.size() - 4; i++) {
		//prechadzam body krivky
		for (double t = 0; t <= 1; t += 0.001) {
			X.x = Y.x;
			X.y = Y.y;
			Y = V[i] * P0(t) + V[i + 1] * P1(t) + V[i + 2] * P2(t) + V[i + 3] * P3(t);
			if (t == 0 && i == 0){X.x = Y.x;X.y = Y.y;}
			//robim usecku medzi 2ma bodmi
			dda(round(X.x), round(X.y), round(Y.x), round(Y.y));
		}
	}
}
//funkcie na vypocet koeficientov
double PaintWidget::P0(double t) {
	double T=-(t*t*t)/6.;
	T += (t*t) / 2.;
	T -= t / 2.;
	T += 1. / 6.;
	return (T);
}

double PaintWidget::P1(double t) {
	double T = (t*t*t) / 2.;
	T -= t*t;
	T += 2. / 3.;
	return (T);
}

double PaintWidget::P2(double t) {
	double T = -(t*t*t) / 2.;
	T += (t*t) / 2.;
	T += t / 2.;
	T += 1. / 6.;
	return (T);
}

double PaintWidget::P3(double t) {
	double T = (t*t*t) / 6.;
	return (T);
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	/*if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());*/
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	/*if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}*/
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

int PaintWidget::selectKth(int * data, int s, int e, int k)
{
	// 5 or less elements: do a small insertion sort
	if (e - s <= 5)
	{
		for (int i = s + 1; i < e; i++)
			for (int j = i; j > 0 && data[j - 1] > data[j]; j--) std::swap(data[j], data[j - 1]);
		return s + k;
	}

	int p = (s + e) / 2; // choose simply center element as pivot

						 // partition around pivot into smaller and larger elements
	std::swap(data[p], data[e - 1]); // temporarily move pivot to the end
	int j = s;  // new pivot location to be calculated
	for (int i = s; i + 1 < e; i++)
		if (data[i] < data[e - 1]) std::swap(data[i], data[j++]);
	std::swap(data[j], data[e - 1]);

	// recurse into the applicable partition
	if (k == j - s) return s + k;
	else if (k < j - s) return selectKth(data, s, j, k);
	else return selectKth(data, j + 1, e, k - j + s - 1); // subtract amount of smaller elements from k
}

